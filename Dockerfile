FROM openjdk:11
ADD target/tweet-service.jar tweet-service.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "tweet-service.jar"]