package com.tweetapp.tweetservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.tweetapp.controller.TweetController;
import com.tweetapp.models.dto.UserDTO;
import com.tweetapp.models.persistence.User;
import com.tweetapp.services.KafkaService;
import com.tweetapp.services.TweetService;
import com.tweetapp.services.UserService;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class TweetControllerTests {

    UserService userService = mock(UserService.class);
    TweetService tweetService = mock(TweetService.class);
    KafkaService kafkaService = mock(KafkaService.class);

    TweetController tweetController = new TweetController(userService, tweetService, kafkaService);

    @Test
    public void testRegisterUserWorks() {
        when(userService.registerUser(any(), any())).thenReturn(new User(null, null, null, null, null));
        ResponseEntity<Object> response = tweetController.registerNewUser(mock(UserDTO.class), "password");
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testRegisterUserReturnsConflictIfUserExists() {
        when(userService.registerUser(any(), any())).thenReturn(null);
        ResponseEntity<Object> response = tweetController.registerNewUser(mock(UserDTO.class), "password");
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
    }
}
