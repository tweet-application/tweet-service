package com.tweetapp.controller;

import javax.validation.Valid;

import com.mongodb.client.result.DeleteResult;
import com.tweetapp.models.dto.TweetDTO;
import com.tweetapp.models.dto.UserDTO;
import com.tweetapp.models.persistence.Tweet;
import com.tweetapp.models.persistence.User;
import com.tweetapp.services.KafkaService;
import com.tweetapp.services.TweetService;
import com.tweetapp.services.UserService;
import com.tweetapp.utils.HttpUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1.0/tweets/")
@CrossOrigin(origins = "*")
public class TweetController {

    private static final String USER_TOPIC = "user";
    private static final String TWEET_TOPIC = "tweet";

    private final UserService userService;
    private final TweetService tweetService;
    private final KafkaService kafkaService;

    public TweetController(UserService userService, TweetService tweetService, KafkaService kafkaService) {
        this.userService = userService;
        this.tweetService = tweetService;
        this.kafkaService = kafkaService;
    }

    @PostMapping("register")
    public ResponseEntity<Object> registerNewUser(@RequestBody @Valid UserDTO userDTO, @RequestHeader String password) {
        User creationResult = userService.registerUser(userDTO.toUser(), password);
        if (creationResult != null) {
            kafkaService.publishMessage(USER_TOPIC, creationResult.getUsername());
            return HttpUtils.send(creationResult, HttpStatus.CREATED);
        } else {
            return HttpUtils.send("Username or email already exists", HttpStatus.CONFLICT);
        }
    }

    @GetMapping("login")
    public ResponseEntity<Object> logIn(@RequestHeader String username, @RequestHeader String password) {
        User loginResult = userService.logIn(username, password);
        return HttpUtils.send(loginResult, loginResult != null ? HttpStatus.OK : HttpStatus.FORBIDDEN);
    }

    @GetMapping("{username}/forgot")
    public ResponseEntity<Object> forgotPassword(@PathVariable String username, @RequestHeader String password) {
        if (userService.forgotPassword(username, password)) {
            return HttpUtils.send(HttpStatus.OK);
        } else {
            return HttpUtils.send(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("users/all")
    public ResponseEntity<Object> getAllUsers(@RequestHeader int pageSize, @RequestHeader int pageNumber) {
        return HttpUtils.send(userService.getAllUsers(pageSize, pageNumber), HttpStatus.OK);
    }

    @GetMapping("user/search")
    public ResponseEntity<Object> searchUser(@RequestParam(defaultValue = "") String username, @RequestHeader int pageSize, @RequestHeader int pageNumber) {
        return HttpUtils.send(userService.searchUser(username,pageSize, pageNumber), HttpStatus.OK);
    }

    @GetMapping("all")
    public ResponseEntity<Object> getAllTweets() {
        return HttpUtils.send(tweetService.getAllTweets(), HttpStatus.OK);
    }

    @GetMapping("{username}")
    public ResponseEntity<Object> getAllUserTweets(@PathVariable String username) {
        return HttpUtils.send(tweetService.getAllUserTweets(username), HttpStatus.OK);
    }

    @PostMapping("{username}/add")
    public ResponseEntity<Object> postTweet(@PathVariable String username, @RequestBody @Valid TweetDTO tweetDTO) {
        Tweet tweet = tweetService.postTweet(username, tweetDTO.toTweet());
        kafkaService.publishMessage(TWEET_TOPIC, tweet.getMessage());
        return HttpUtils.send(tweet, HttpStatus.OK);
    }

    @PutMapping("{username}/update/{id}")
    public ResponseEntity<Object> updateTweet(@PathVariable String username, @PathVariable String id,
            @RequestBody @Valid TweetDTO tweetDTO) {
        return HttpUtils.send(tweetService.updateTweet(username, id, tweetDTO.toTweet()), HttpStatus.OK);
    }

    @DeleteMapping("{username}/delete/{id}")
    public ResponseEntity<Object> deleteTweet(@PathVariable String username, @PathVariable String id) {
        DeleteResult deleteResult = tweetService.deleteTweet(username, id);
        return HttpUtils.send(deleteResult,
                deleteResult.getDeletedCount() > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);

    }

    @PutMapping("{username}/like/{id}")
    public ResponseEntity<Object> likeTweet(@PathVariable String username, @PathVariable String id) {
        tweetService.likeTweet(username, id);
        return HttpUtils.send(HttpStatus.OK);
    }

    @PostMapping("{username}/reply/{id}")
    public ResponseEntity<Object> replyToTweet(@PathVariable String username, @PathVariable String id,
            @RequestBody @Valid TweetDTO tweetDTO) {
        return HttpUtils.send(tweetService.replyToTweet(username, id, tweetDTO.toTweet()), HttpStatus.CREATED);
    }

    @GetMapping("{id}/replies")
    public ResponseEntity<Object> getTweetReplies(@PathVariable String id) {
        return HttpUtils.send(tweetService.getTweetReplies(id), HttpStatus.OK);
    }

}
