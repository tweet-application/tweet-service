package com.tweetapp.utils;

import org.apache.commons.codec.digest.DigestUtils;

public final class SecurityUtils {

    private SecurityUtils(){
        throw new IllegalStateException("Cannot instantiate utility classes");
    }
    
    public static String hash(String originalString){
            return DigestUtils.sha3_256Hex(originalString);
    }
}
