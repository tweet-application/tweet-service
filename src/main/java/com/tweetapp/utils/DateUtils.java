package com.tweetapp.utils;

import java.time.Instant;

import org.ocpsoft.prettytime.PrettyTime;

public class DateUtils {
    public static String prettyTime(Instant time) {
        return new PrettyTime().format(time);
    }
}
