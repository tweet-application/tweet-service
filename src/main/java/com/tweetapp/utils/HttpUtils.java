package com.tweetapp.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class HttpUtils {

    private HttpUtils(){
        throw new IllegalStateException("Cannot instantiate utility classes");
    }

    public static ResponseEntity<Object> send(Object object, HttpStatus httpStatus) {
        return new ResponseEntity<>(object, httpStatus);
    }

    public static ResponseEntity<Object> send(HttpStatus httpStatus) {
        return new ResponseEntity<>(httpStatus);
    }
}
