package com.tweetapp.models.response;

import java.util.ArrayList;
import java.util.List;

import com.tweetapp.models.persistence.Tweet;
import com.tweetapp.utils.DateUtils;

public class TweetResponse {
    private String id;

    private String username;

    private String message;

    private String tag;

    private String timeAgo;

    private List<String> likedBy = new ArrayList<>();

    public TweetResponse(){}

    public TweetResponse(Tweet tweet) {
        this.id = tweet.getId();
        this.username = tweet.getUsername();
        this.message = tweet.getMessage();
        this.tag = tweet.getTag();
        this.timeAgo = DateUtils.prettyTime(tweet.getCreatedDt());
        this.likedBy = tweet.getLikedBy();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public List<String> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(List<String> likedBy) {
        this.likedBy = likedBy;
    }

    

}
