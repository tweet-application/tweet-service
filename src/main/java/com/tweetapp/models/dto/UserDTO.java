package com.tweetapp.models.dto;

import javax.validation.constraints.NotEmpty;

import com.tweetapp.models.persistence.User;

import org.springframework.validation.annotation.Validated;

@Validated
public class UserDTO {
    
    @NotEmpty(message = "username field cannot be null")
    private String username;

    @NotEmpty(message = "firstName field cannot be null")
    private String firstName;

    @NotEmpty(message = "lastName field cannot be null")
    private String lastName;

    @NotEmpty(message = "email field cannot be null")
    private String email;

    @NotEmpty(message = "contactNumber field cannot be null")
    private String contactNumber;

    public User toUser(){
        return new User(getUsername(), getFirstName(), getLastName(), getEmail(), getContactNumber());
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getContactNumber() {
        return contactNumber;
    }
    
}
