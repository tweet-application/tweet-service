package com.tweetapp.models.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.tweetapp.models.persistence.Tweet;

import org.springframework.validation.annotation.Validated;

@Validated
public class TweetDTO {

    @NotEmpty(message = "username cannot be empty")
    private String username;

    @NotEmpty(message = "message cannot be empty")
    @Size(max = 144)
    private String message;

    @Size(max = 50)
    private String tag;

    public Tweet toTweet() {
        return new Tweet(getUsername(), getMessage(), getTag());
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    public String getTag() {
        return tag;
    }
    
}
