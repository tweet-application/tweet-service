package com.tweetapp.models.persistence;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// TODO add valdiations
@Document
public class Tweet {

    @Id
    private String id;

    private String username;

    private String message;

    private String tag;

    private Instant createdDt = Instant.now();

    private Instant updatedDt;

    private List<String> likedBy = new ArrayList<>();

    public Tweet(String username, String message, String tag) {
        this.username = username;
        this.message = message;
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Instant getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Instant createdDt) {
        this.createdDt = createdDt;
    }

    public Instant getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Instant updatedDt) {
        this.updatedDt = updatedDt;
    }

    public List<String> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(List<String> likedBy) {
        this.likedBy = likedBy;
    }

}
