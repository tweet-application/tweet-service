package com.tweetapp.services;

import com.tweetapp.models.persistence.User;
import com.tweetapp.repositories.UserRepository;
import com.tweetapp.utils.SecurityUtils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User registerUser(User user, String password) {
        password = SecurityUtils.hash(password);
        return userRepository.createUser(user, password);
    }

    public User logIn(String username, String password) {
        password = SecurityUtils.hash(password);
        return userRepository.findByUsernameAndPassword(username, password);
    }

    public boolean forgotPassword(String username, String password) {
        password = SecurityUtils.hash(password);
        return userRepository.forgotPassword(username, password);
    }

    public Page<User> getAllUsers(int pageSize, int pageNumber) {
        return userRepository.findAll(Pageable.ofSize(pageSize).withPage(pageNumber));
    }

    public Page<User> searchUser(String username, int pageSize, int pageNumber) {
        return userRepository.findByUsername(username, Pageable.ofSize(pageSize).withPage(pageNumber));
    }

}
