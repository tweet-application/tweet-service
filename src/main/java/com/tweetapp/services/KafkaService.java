package com.tweetapp.services;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class KafkaService {
    
    private KafkaTemplate<String, String> kafkaTemplate;
    
    public KafkaService(KafkaTemplate<String, String> kafkaTemplate){
        this.kafkaTemplate = kafkaTemplate;
    }

    public void publishMessage(String topic, String message) {
        log.info("Kafka publisher: publishing message: '" + message + "' to topic: '" + topic + "'");
        kafkaTemplate.send(topic, message);
    }

    @KafkaListener(topics = {"user"}, groupId = "exampleGroupId")
    public void consumeMessage(String user) {
        log.info("Kafka subscriber: User created: " + user);
    }

    @KafkaListener(topics = {"tweet"}, groupId = "exampleGroupId")
    public void consumeMessagee(String tweet) {
        log.info("Kafka subscriber: Tweet posted: " + tweet);
    }
}
