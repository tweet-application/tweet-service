package com.tweetapp.services;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.tweetapp.models.persistence.Tweet;
import com.tweetapp.models.response.TweetResponse;
import com.tweetapp.repositories.TweetRepository;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class TweetService {

	public static final int ID_LENGTH = 8;

	private TweetRepository tweetRepository;

	public TweetService(TweetRepository tweetRepository) {
		this.tweetRepository = tweetRepository;
	}

	public List<TweetResponse> getAllTweets() {
		List<TweetResponse> tweets = new ArrayList<>();
		tweetRepository.getAllTweets().forEach(tweet -> tweets.add(new TweetResponse(tweet)));
		return tweets;
	}

	public List<TweetResponse> getAllUserTweets(String username) {
		List<TweetResponse> tweets = new ArrayList<>();
		tweetRepository.findTweetsByUsername(username).forEach(tweet -> tweets.add(new TweetResponse(tweet)));
		return tweets;
	}

	public Tweet postTweet(String username, Tweet tweet) {
		tweet.setId(randomString());
		tweet.setUsername(username);
		return tweetRepository.insert(tweet);
	}

	public TweetResponse updateTweet(String username, String id, Tweet tweet) {
		return new TweetResponse(tweetRepository.updateTweet(username, id, tweet));
	}

	public DeleteResult deleteTweet(String username, String id) {
		return tweetRepository.deleteTweet(username, id);
	}

	public UpdateResult likeTweet(String username, String id) {
		return tweetRepository.likeTweet(username, id);
	}

	public TweetResponse replyToTweet(String username, String id, Tweet tweet) {
		tweet.setId(new StringBuilder().append(id).append(',').append(randomString()).toString());
		tweet.setUsername(username);
		return new TweetResponse(tweetRepository.insert(tweet));
	}

	public List<TweetResponse> getTweetReplies(String id) {
		List<TweetResponse> tweets = new ArrayList<>();
		tweetRepository.getTweetReplies(id).forEach(tweet -> tweets.add(new TweetResponse(tweet)));
		return tweets;
	}

	private static String randomString() {
		return RandomStringUtils.randomAlphanumeric(ID_LENGTH);
	}

}
