package com.tweetapp.repositories.custom;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.tweetapp.models.persistence.Tweet;

public interface CustomTweetRepository {
    
    Tweet updateTweet(String username, String id, Tweet tweet);

    DeleteResult deleteTweet(String username, String id);

    UpdateResult likeTweet(String username, String id);

}
