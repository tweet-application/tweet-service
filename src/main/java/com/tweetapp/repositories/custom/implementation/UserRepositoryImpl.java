package com.tweetapp.repositories.custom.implementation;

import com.tweetapp.models.persistence.User;
import com.tweetapp.repositories.custom.CustomUserRepository;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class UserRepositoryImpl implements CustomUserRepository {

    private final MongoOperations mongoOperations;

    public UserRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public User createUser(User user, String password) {
        return mongoOperations
                .upsert(new Query().addCriteria(Criteria.where("_id").is(user.getUsername())),
                        new Update().setOnInsert("_id", user.getUsername()).setOnInsert("password", password)
                                .setOnInsert("firstName", user.getFirstName())
                                .setOnInsert("lastName", user.getLastName()).setOnInsert("email", user.getEmail())
                                .setOnInsert("contactNumber", user.getContactNumber()),
                        User.class)
                .getUpsertedId() != null ? user : null;
    }

    public boolean forgotPassword(String username, String newPassword) {
        return mongoOperations.updateFirst(new Query().addCriteria(Criteria.where("_id").is(username)),
                new Update().set("password", newPassword), User.class).getModifiedCount() == 1;
    }

}
