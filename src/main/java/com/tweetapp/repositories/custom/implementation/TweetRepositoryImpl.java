package com.tweetapp.repositories.custom.implementation;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.tweetapp.models.persistence.Tweet;
import com.tweetapp.repositories.custom.CustomTweetRepository;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class TweetRepositoryImpl implements CustomTweetRepository {

    private final MongoOperations mongoOperations;

    public TweetRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public Tweet updateTweet(String username, String id, Tweet tweet) {
        return mongoOperations.findAndModify(new Query().addCriteria(Criteria.where("username").is(username).and("_id").is(id)),
                new Update().set("message", tweet.getMessage()).set("tag", tweet.getTag()).set("updatedDt",
                        tweet.getCreatedDt()),
                Tweet.class);
    }

    @Override
    public DeleteResult deleteTweet(String username, String id) {
        return mongoOperations.remove(
                new Query().addCriteria(Criteria.where("username").is(username).and("_id").regex(id)), Tweet.class);
    }

    @Override
    public UpdateResult likeTweet(String username, String id) {
        return mongoOperations.updateFirst(
                new Query().addCriteria(Criteria.where("username").is(username).and("_id").is(id)),
                new Update().addToSet("likedBy", username), Tweet.class);
    }

}
