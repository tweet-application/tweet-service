package com.tweetapp.repositories.custom;

import com.tweetapp.models.persistence.User;

public interface CustomUserRepository {

    User createUser(User user, String password);
    
    boolean forgotPassword(String username, String password);

}
