package com.tweetapp.repositories;

import com.tweetapp.models.persistence.User;
import com.tweetapp.repositories.custom.CustomUserRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String>, CustomUserRepository {

    @Query("{ username: { $regex: ?0, $options: i }}")
    Page<User> findByUsername(String username, Pageable pageable);

    @Query(" { $and: [{ username: ?0 }, { password: ?1 }] } ")
    User findByUsernameAndPassword(String username, String password);

}
