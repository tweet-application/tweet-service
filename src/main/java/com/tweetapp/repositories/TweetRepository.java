package com.tweetapp.repositories;

import java.util.List;

import com.tweetapp.models.persistence.Tweet;
import com.tweetapp.repositories.custom.CustomTweetRepository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface TweetRepository extends MongoRepository<Tweet, String>, CustomTweetRepository {
    
    @Query("{ _id: { $not: /,/ } }")
    List<Tweet> getAllTweets();

    @Query("{ username: ?0, _id: { $not: /,/ } }")
    List<Tweet> findTweetsByUsername(String username);

    @Query("{ _id: { $regex: /^?0,........$/ } }")
    List<Tweet> getTweetReplies(String id);
}
