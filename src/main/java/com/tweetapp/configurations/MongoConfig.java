package com.tweetapp.configurations;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.tweetapp.repositories")
public class MongoConfig extends AbstractMongoClientConfiguration {

    // TODO set up environments
    @Value("${spring.data.mongodb.uri}")
    private String connectionURI;

    @Value("${spring.data.mongodb.database}")
    private String dbName;

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create(
                MongoClientSettings.builder().applyConnectionString(new ConnectionString(connectionURI)).build());
    }

    @Bean
    public MongoOperations mongoOperations() {
        return new MongoTemplate(mongoClient(), dbName);
    }

}
