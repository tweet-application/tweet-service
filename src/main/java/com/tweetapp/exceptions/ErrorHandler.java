package com.tweetapp.exceptions;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.tweetapp.utils.HttpUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.KafkaException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getSimpleName());

    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ResponseEntity<Object> validationExceptionResponse(MethodArgumentNotValidException e) {

        LOGGER.log(Level.WARNING, "Error caught by exception handler", e.getFieldErrors());

        Map<String, String> errors = new HashMap<>();

        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return HttpUtils.send(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {

        LOGGER.log(Level.WARNING, "Error caught by exception handler", e);

        if (e instanceof KafkaException) {
            return HttpUtils.send(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return HttpUtils.send(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

}
